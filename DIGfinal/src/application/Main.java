package application;

import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.container.DefaultFlowContainer;

import java.io.IOException;
import java.io.Serializable;

import application.controlador.VistaEmpleadoController;
import application.controlador.VistaLoginController;
import application.controlador.VistaRootController;
import application.modelo.Empleado;
import application.modelo.Venta;
import application.persistencia.EmpleadoDAOJPAImpl;
import application.persistencia.VentaDAOJPAImpl;
import application.util.Seguridad;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class Main extends Application {

	public FlowHandler mainFlowHandler;
	public Stage primaryStage;

	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage = primaryStage;
		try {
			Flow flow = new Flow(VistaLoginController.class);
			mainFlowHandler = flow.createHandler();
			StackPane pane = mainFlowHandler.start();
			VistaLoginController controladorController = (VistaLoginController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorController.setFlow(mainFlowHandler);
			Scene scene = new Scene(pane);
			scene.getStylesheets().add("./application/application.css");
			primaryStage.setResizable(false);
			primaryStage.setTitle("Sistema Dise�o de Interfaz Grafica");
			this.primaryStage.setScene(scene);
			this.primaryStage.show();
		} catch (FlowException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}

}
