package application.controlador.dialogs;

import javax.annotation.PostConstruct;

import application.controlador.VistaClienteController;
import application.controlador.VistaEmpleadoController;
import application.modelo.Cliente;
import application.modelo.Empleado;
import application.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;

@FXMLController(value = "../../vista/dialogs/DialogCliente.fxml")
public class DialogClienteController {
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textApellido;
	@FXML
	private TextField textTelefono;
	@FXML
	private TextField textDireccion;
	@FXML
	private TextField textCuilCuit;
	@FXML
	private TextField textMail;
	@FXML
	private ComboBox<String> comboTipo;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;

	
	private VistaClienteController controladorVentanaPrincipal;
	private Cliente cliente;
	private ObservableList<String> itemsCombo;
	Stage stage;
	
	@PostConstruct
	public void init() {
		itemsCombo = FXCollections.observableArrayList();
		itemsCombo.addAll("consumidor final", "responsable inscripto");
		this.comboTipo.setItems(itemsCombo);
	}
	
	@ActionMethod("aceptar")
	public void nuevoEmpleado() {
		if(validar()){
			if(cliente == null) {
				cliente = new Cliente();
			}
			cliente.setNombre(textNombre.getText());
			cliente.setApellido(textApellido.getText());
			cliente.setDireccion(textDireccion.getText());
			cliente.setCuilCuit(textCuilCuit.getText());
			cliente.setTelefono(textTelefono.getText());
			cliente.setMail(textMail.getText());
			cliente.setTipo((String) comboTipo.getSelectionModel().getSelectedItem());
			stage.close();
		}
	}
	
	@ActionMethod("cancelar")
	public void cancelar() {
		stage.close();
	}
	
	public boolean validar() {
		if(
				Validate.textFieldValidate(textApellido, "apellido") &&
				Validate.textFieldValidate(textDireccion, "direccion") &&
				Validate.textFieldValidate(textMail, "mail") &&
				Validate.textFieldValidate(textNombre, "nombre") &&
				Validate.textFieldNumberValidate(textTelefono, "telefono") &&
				Validate.comboValidate(comboTipo, "tipo") &&
				Validate.ExpresionTextFieldValidate(textCuilCuit, "[0-9]{11}", "Cuil Cuit", "11 numeros sin espacios")
				) {
			return true;
		}
		return false;
	}
	
	public void llenarCampos(Cliente cli) {
		try {
			this.textApellido.setText(cli.getApellido());
			this.textCuilCuit.setText(cli.getCuilCuit());
			this.textDireccion.setText(cli.getDireccion());
			this.textMail.setText(cli.getMail());
			this.textNombre.setText(cli.getNombre());
			this.textTelefono.setText(cli.getTelefono());
			this.comboTipo.getSelectionModel().select(cli.getTipo());
		} catch (NullPointerException e) {
			
		}
	}
	
	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public void setControladorVentanaPrincipal(VistaClienteController controlador) {
		this.controladorVentanaPrincipal = controlador;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
}
