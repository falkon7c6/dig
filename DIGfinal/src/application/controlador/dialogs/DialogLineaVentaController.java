package application.controlador.dialogs;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import com.mysql.jdbc.StringUtils;

import application.controlador.VistaJuegoController;
import application.controlador.VistaVentaController;
import application.modelo.Juego;
import application.modelo.Lineaventa;
import application.persistencia.JuegoDAOJPAImpl;
import application.util.Validate;
import application.util.archivoUtil;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ListChangeListener.Change;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.stage.Stage;

@FXMLController(value = "../../vista/dialogs/DialogLineaVenta.fxml")
public class DialogLineaVentaController {
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	private TableView<Juego> tabla;
	@FXML
	private TableColumn<Juego, String> colID;
	@FXML
	private TableColumn<Juego, String> colNombre;
	@FXML
	private TableColumn<Juego, String> colPlataforma;
	@FXML
	private TableColumn<Juego, String> colDesarrollador;
	@FXML
	private TableColumn<Juego, String> colStock;
	@FXML
	private TableColumn<Juego, String> colPrecio;
	@FXML
	private TextField textFiltrar;
	@FXML
	private TextField textCantidad;
	@FXML
	private TextField textSubtotal;

	private VistaVentaController controladorVentanaPrincipal;
	private Lineaventa lineaVenta;
	private JuegoDAOJPAImpl daoJuego;
	private ObservableList<Juego> itemsTabla;
	Stage stage;

	@PostConstruct
	public void init() {
		daoJuego = new JuegoDAOJPAImpl();
		itemsTabla = FXCollections.observableArrayList();
		iniciarColumnas();
		itemsTabla.setAll(daoJuego.buscarTodos());
		tabla.setItems(itemsTabla);
		textCantidad.setOnKeyReleased(event-> {
			if(StringUtils.isEmptyOrWhitespaceOnly(textCantidad.getText())) {
				textSubtotal.setText("" + 0);
			} else {
				calcularSubtotal();
			}
		});
	}

	private void iniciarColumnas() {
		this.colID.setCellValueFactory(new PropertyValueFactory<>("idJuego"));
		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));
		this.colPlataforma.setCellValueFactory(new PropertyValueFactory<>(
				"plataforma"));
		this.colPrecio
				.setCellValueFactory(new PropertyValueFactory<>("precio"));
		this.colStock.setCellValueFactory(new PropertyValueFactory<>("stock"));
		this.colDesarrollador.setCellValueFactory(new PropertyValueFactory<>(
				"desarrollador"));
		tabla.getSelectionModel().getSelectedIndices()
		.addListener(new ListChangeListener<Integer>() {
			@Override
			public void onChanged(Change<? extends Integer> change) {
				if (change.getList().size() >= 1) {
					calcularSubtotal();
					
				}
			}
		});
	}

	@ActionMethod("aceptar")
	public void nuevoJuego() {
		if (validar()) {
			if (lineaVenta == null) {
				lineaVenta = new Lineaventa();
			}
			lineaVenta.setJuego(tabla.getSelectionModel().getSelectedItem());
			lineaVenta.setCantidad(Integer.parseInt(textCantidad.getText()));
			BigDecimal subtotal = new BigDecimal(lineaVenta.getJuego().getPrecio()
					.doubleValue()
					* lineaVenta.getCantidad());
			lineaVenta.setSubtotal(subtotal);
			stage.close();
		}
	}

	public void calcularSubtotal() {
		try {
			double subtotal = 0d;
			subtotal = Integer.parseInt(textCantidad.getText())
					* tabla.getSelectionModel().getSelectedItem().getPrecio()
							.doubleValue();
			textSubtotal.setText("" + subtotal);
		} catch (Exception e) {
		}
	}
	
	public boolean validar() {
		if(
			Validate.tableNotSelectedItemValidate(tabla)
			&& Validate.textFieldNumberValidate(textCantidad, "cantidad")
				) {
			return true;
		}
		return false;
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		stage.close();
	}

	public Lineaventa getLineaventa() {
		return lineaVenta;
	}

	public void setLineaventa(Lineaventa lineaventa) {
		this.lineaVenta = lineaventa;
	}

	public void setControladorVentanaPrincipal(VistaVentaController controlador) {
		this.controladorVentanaPrincipal = controlador;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
