package application.controlador;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import application.controlador.dialogs.DialogJuegoController;
import application.modelo.Juego;
import application.persistencia.JuegoDAOJPAImpl;
import application.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

@FXMLController(value = "../vista/VistaJuego.fxml")
public class VistaJuegoController {
	@FXML
	private TableView<Juego> tabla;
	@FXML
	private TableColumn<Juego, String> colId;
	@FXML
	private TableColumn<Juego, String> colNombre;
	@FXML
	private TableColumn<Juego, String> colPlataforma;
	@FXML
	private Label labelNombre;
	@FXML
	private Label labelDescripcion;
	@FXML
	private Label labelGenero;
	@FXML
	private Label labelPlataforma;
	@FXML
	private Label labelDesarrollador;
	@FXML
	private Label labelDistribuidor;
	@FXML
	private Label labelFecha;
	@FXML
	private Label labelStock;
	@FXML
	private Label labelPrecio;
	@FXML
	private ImageView imagen;
	@FXML
	@ActionTrigger("nuevoJuego")
	private Button botonAceptar;
	@FXML
	@ActionTrigger("editarJuego")
	private Button botonEditar;
	@FXML
	@ActionTrigger("borrarJuego")
	private Button botonEliminar;
	@FXML
	private AnchorPane panelDerechoJuegoAnchorPane;

	private ObservableList<Juego> itemsTabla;
	JuegoDAOJPAImpl dao;

	@PostConstruct
	public void onInit() {
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		dao = new JuegoDAOJPAImpl();
		itemsTabla.setAll(dao.buscarTodos());
		tabla.setItems(itemsTabla);
		vaciarEspacios();

	}

	public void iniciarColumnas() {
		this.colId.setCellValueFactory(new PropertyValueFactory<>("idJuego"));

		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));

		this.colPlataforma.setCellValueFactory(new PropertyValueFactory<>(
				"plataforma"));

		tabla.getSelectionModel().getSelectedIndices()
				.addListener(new ListChangeListener<Integer>() {
					@Override
					public void onChanged(Change<? extends Integer> change) {
						if (change.getList().size() >= 1) {
							if (tabla.getSelectionModel().getSelectedItem() != null) {
								cargarDatos(tabla.getSelectionModel()
										.getSelectedItem());
							} else {
								vaciarEspacios();
							}
						}
					}
				});
	}

	@ActionMethod("nuevoJuego")
	public void nuevoJuego() {

		Juego juego = iniciarDialog(null);
		if (juego != null) {
			dao.insertar(juego);
			itemsTabla.add(juego);
			cargarDatos(juego);
		}

	}

	@ActionMethod("borrarJuego")
	public void borrarCliente() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			Juego temp = tabla.getSelectionModel().getSelectedItem();
			dao.borrar(temp);
			itemsTabla.remove(temp);
		}
	}

	@ActionMethod("editarJuego")
	public void editarCliente() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			Juego temp = tabla.getSelectionModel().getSelectedItem();
			temp = iniciarDialog(temp);
			dao.salvar(temp);
			cargarDatos(temp);
		}
	}

	public void cargarDatos(Juego juego) {
		try {
			this.labelDesarrollador.setText(juego.getDesarrollador());
			this.labelDistribuidor.setText(juego.getDistribuidor());
			this.labelFecha.setText(juego.getFecha());
			this.labelGenero.setText(juego.getGenero());
			this.labelNombre.setText(juego.getNombre());
			this.labelPlataforma.setText(juego.getPlataforma());
			this.labelStock.setText("" + juego.getStock());
			this.labelDescripcion.setText(juego.getDescripcion());
			this.labelPrecio.setText("" + juego.getPrecio());
			cargarImagen(juego);
		} catch (NullPointerException e) {
			System.out.println("null");
		}
	}

	public void vaciarEspacios() {
		this.labelDesarrollador.setText("");
		this.labelDistribuidor.setText("");
		this.labelFecha.setText("");
		this.labelGenero.setText("");
		this.labelNombre.setText("");
		this.labelPlataforma.setText("");
		this.labelStock.setText("");
		this.labelDescripcion.setText("");
		// TODO imagen por defecto
	}

	public void cargarImagen(Juego juego) {
		BufferedImage img;
		try {
			img = ImageIO.read(new ByteArrayInputStream(juego.getImagen()));
			Image image = SwingFXUtils.toFXImage(img, null);
			this.imagen.setImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException ex) {
			System.out.println("sin imagen");
		}
	}

	public Juego iniciarDialog(Juego juego) {
		DialogJuegoController controladorDialog;
		Stage stage = new Stage();
		AnchorPane root;
		try {
			Flow mainFlow = new Flow(DialogJuegoController.class);
			FlowHandler mainFlowHandler = mainFlow.createHandler();
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			controladorDialog = (DialogJuegoController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			Scene scene = new Scene(pane);
			scene.getStylesheets().add("/application/application.css");
			stage.setScene(scene);
			stage.setTitle("Juego");
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setStage(stage);
			if (juego != null) {
				controladorDialog.setJuego(juego);
				controladorDialog.llenarCampos(juego);
			}
			stage.showAndWait();
			return controladorDialog.getJuego();

		} catch (FlowException e) {
			e.printStackTrace();
		}

		return null;
	}

}
