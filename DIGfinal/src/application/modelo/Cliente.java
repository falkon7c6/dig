package application.modelo;

import java.io.Serializable;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.*;

import java.util.List;

/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;
	private SimpleIntegerProperty idCliete = new SimpleIntegerProperty();
	private SimpleStringProperty cuilCuit = new SimpleStringProperty();
	private SimpleStringProperty apellido = new SimpleStringProperty();
	private SimpleStringProperty direccion = new SimpleStringProperty();
	private SimpleStringProperty mail = new SimpleStringProperty();
	private SimpleStringProperty nombre = new SimpleStringProperty();
	private SimpleStringProperty telefono = new SimpleStringProperty();
	private SimpleStringProperty tipo = new SimpleStringProperty();
	private List<Venta> pedidos;

	public Cliente() {
	}

	public final SimpleIntegerProperty idClieteProperty() {
		return this.idCliete;
	}

	@Override
	public String toString() {
		return "id: " + idCliete.get() + ", " + cuilCuit.get() + ", "
				+ apellido.get() + ", " + nombre.get();
	}

	// bi-directional many-to-one association to Pedido

	@OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL)
	public List<Venta> getPedidos() {
		return this.pedidos;
	}

	public void setPedidos(List<Venta> pedidos) {
		this.pedidos = pedidos;
	}

	public Venta addPedido(Venta pedido) {
		getPedidos().add(pedido);
		pedido.setCliente(this);

		return pedido;
	}

	public Venta removePedido(Venta pedido) {
		getPedidos().remove(pedido);
		pedido.setCliente(null);
		return pedido;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public final int getIdCliete() {
		return this.idClieteProperty().get();
	}

	public final void setIdCliete(final int idCliete) {
		this.idClieteProperty().set(idCliete);
	}

	public final SimpleStringProperty cuilCuitProperty() {
		return this.cuilCuit;
	}

	public final java.lang.String getCuilCuit() {
		return this.cuilCuitProperty().get();
	}

	public final void setCuilCuit(final java.lang.String cuilCuit) {
		this.cuilCuitProperty().set(cuilCuit);
	}

	public final SimpleStringProperty direccionProperty() {
		return this.direccion;
	}

	public final java.lang.String getDireccion() {
		return this.direccionProperty().get();
	}

	public final void setDireccion(final java.lang.String direccion) {
		this.direccionProperty().set(direccion);
	}

	public final SimpleStringProperty mailProperty() {
		return this.mail;
	}

	public final java.lang.String getMail() {
		return this.mailProperty().get();
	}

	public final void setMail(final java.lang.String mail) {
		this.mailProperty().set(mail);
	}

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	public final SimpleStringProperty telefonoProperty() {
		return this.telefono;
	}

	public final java.lang.String getTelefono() {
		return this.telefonoProperty().get();
	}

	public final void setTelefono(final java.lang.String telefono) {
		this.telefonoProperty().set(telefono);
	}

	public final SimpleStringProperty tipoProperty() {
		return this.tipo;
	}

	public final java.lang.String getTipo() {
		return this.tipoProperty().get();
	}

	public final void setTipo(final java.lang.String tipo) {
		this.tipoProperty().set(tipo);
	}

	public final SimpleStringProperty apellidoProperty() {
		return this.apellido;
	}

	public final java.lang.String getApellido() {
		return this.apellidoProperty().get();
	}

	public final void setApellido(final java.lang.String apellido) {
		this.apellidoProperty().set(apellido);
	}

	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy=GenerationType.IDENTITY) public int
	 * getIdCliete() { return this.idCliete; }
	 * 
	 * public void setIdCliete(int idCliete) { this.idCliete = idCliete; }
	 * 
	 * 
	 * public String getCuilCuit() { return this.cuilCuit; }
	 * 
	 * public void setCuilCuit(String cuilCuit) { this.cuilCuit = cuilCuit; }
	 * 
	 * 
	 * public String getDescripcion() { return this.descripcion; }
	 * 
	 * public void setDescripcion(String descripcion) { this.descripcion =
	 * descripcion; }
	 * 
	 * 
	 * public String getDireccion() { return this.direccion; }
	 * 
	 * public void setDireccion(String direccion) { this.direccion = direccion;
	 * }
	 * 
	 * 
	 * public String getMail() { return this.mail; }
	 * 
	 * public void setMail(String mail) { this.mail = mail; }
	 * 
	 * 
	 * public String getNombre() { return this.nombre; }
	 * 
	 * public void setNombre(String nombre) { this.nombre = nombre; }
	 * 
	 * 
	 * public String getTelefono() { return this.telefono; }
	 * 
	 * public void setTelefono(String telefono) { this.telefono = telefono; }
	 * 
	 * 
	 * public String getTipo() { return this.tipo; }
	 * 
	 * public void setTipo(String tipo) { this.tipo = tipo; }
	 * 
	 * 
	 * //bi-directional many-to-one association to Pedido
	 * 
	 * @OneToMany(mappedBy="cliente") public List<Pedido> getPedidos() { return
	 * this.pedidos; }
	 * 
	 * public void setPedidos(List<Pedido> pedidos) { this.pedidos = pedidos; }
	 * 
	 * public Pedido addPedido(Pedido pedido) { getPedidos().add(pedido);
	 * pedido.setCliente(this);
	 * 
	 * return pedido; }
	 * 
	 * public Pedido removePedido(Pedido pedido) { getPedidos().remove(pedido);
	 * pedido.setCliente(null);
	 * 
	 * return pedido; }
	 */

}