package application.util;

import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class Validate {

	public static boolean textFieldValidate(TextField text, String nombre) {
		/*
		 * devuelve true si el textfield no esta vacio o no inicia con espacios
		 * en blanco
		 */
		nombre = nombre.toUpperCase();
		if (text.getText() != null && text.getText().length() != 0
				&& text.getText().matches("^[^\\s].*")) {
			return true;
		}
		DialogsUtil.errorDialog("Validar", "el campo ingresado en " + nombre
				+ " esta vacio o contiene espacios en blanco" + "al principio",
				text.getText());
		return false;
	}

	public static boolean textFieldNumberValidate(TextField text, String nombre) {
		/*
		 * devuelve true si el textfield no esta vacio o no inicia con espacios
		 * en blanco
		 */
		nombre = nombre.toUpperCase();
		if (text.getText() != null && text.getText().length() != 0
				&& text.getText().matches("[\\d]*")) {
			return true;
		}
		DialogsUtil.errorDialog("Validar", "el campo ingresado en " + nombre
				+ " esta vacio o no es un numero", text.getText());
		return false;
	}

	public static boolean textFieldDecimalValidate(TextField text, String nombre) {
		/*
		 * devuelve true si el textfield no esta vacio o no inicia con espacios
		 * en blanco
		 */
		nombre = nombre.toUpperCase();
		if (text.getText() != null && text.getText().length() != 0
				&& (text.getText().matches("[0-9]*[\\.][0-9]{0,2}") || text.getText().matches("[0-9]*"))) {
			return true;
		}
		DialogsUtil
				.errorDialog(
						"Validar",
						"el campo ingresado en "
								+ nombre
								+ " esta vacio o no respeta el formato decimal: ",
						""
								+ "Numero entero, seguido del caracter . (punto) ,seguido de 2 numeros");
		return false;
	}

	public static boolean ExpresionTextFieldValidate(TextField text,
			String expresion, String nombre) {
		/*
		 * devuelve true si el textfield no esta vacio o no inicia con espacios
		 * en blanco
		 */
		nombre = nombre.toUpperCase();
		if (text.getText() != null && text.getText().length() != 0
				&& text.getText().matches(expresion)) {
			return true;
		}
		DialogsUtil.errorDialog("Validar", "el campo ingresado en " + nombre
				+ " esta vacio o no respeta el formato", "");
		return false;
	}

	public static boolean ExpresionTextFieldValidate(TextField text,
			String expresion, String nombre, String formato) {
		/*
		 * devuelve true si el textfield no esta vacio o no inicia con espacios
		 * en blanco
		 */
		nombre = nombre.toUpperCase();
		if (text.getText() != null && text.getText().length() != 0
				&& text.getText().matches(expresion)) {
			return true;
		}
		DialogsUtil.errorDialog("Validar", "el campo ingresado en " + nombre
				+ " esta vacio o no respeta el formato: ", formato);
		return false;
	}

	public static boolean comboValidate(ComboBox<?> combo, String nombre) {
		/*
		 * devuelve true si el textfield no esta vacio o no inicia con espacios
		 * en blanco
		 */
		nombre = nombre.toUpperCase();
		if (combo.getSelectionModel().getSelectedIndex() > -1) {
			return true;
		}
		DialogsUtil.errorDialog("Validar",
				"No selecciono ningun item del combobox: ", " " + nombre);
		return false;
	}

	public static boolean datePickerValidate(DatePicker date, String nombre) {
		/*
		 * devuelve true si el textfield no esta vacio o no inicia con espacios
		 * en blanco
		 */
		if (date.getValue() != null) {
			return true;
		}
		DialogsUtil.errorDialog("Validar", "el campo ingresado en " + nombre
				+ " esta vacio o no respeta el formato de fecha",
				"mm/dd/aa -> mes/dia/anio");
		return false;
	}
	
	public static boolean textAreaValidate(TextArea text, String nombre, int max) {
		if(text.getText() != null && text.getText().length() <= max) {
			System.out.println("TEXTO: " + text.getText());
			return true;
		}
		DialogsUtil.errorDialog("Validar", "el campo ingresado en " + nombre
				+ " esta vacio", "");
		return false;
	}
	
	public static boolean tableValidate(TableView tabla, String nombre) {
		if(!tabla.getItems().isEmpty()) {
			return true;
		}
		DialogsUtil.errorDialog("Validar", "la tabla " + nombre
				+ " esta vacia", "");
		return false;
	}
	
	public static boolean tableNotSelectedItemValidate(TableView tabla) {
		if(tabla.getSelectionModel().getSelectedIndex() > -1) {
			return true;
		}
		DialogsUtil.errorDialog("Validar", "no se selecciono nada en la tabla"
				, "");
		return false;
	}
}
