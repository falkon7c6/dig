package application.controlador;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;

import org.eclipse.persistence.descriptors.ReturningPolicy;

import application.controlador.dialogs.DialogJuegoController;
import application.controlador.dialogs.DialogLineaVentaController;
import application.modelo.Cliente;
import application.modelo.Juego;
import application.modelo.Lineaventa;
import application.modelo.Venta;
import application.persistencia.ClienteDAOJPAImpl;
import application.persistencia.EmpleadoDAOJPAImpl;
import application.persistencia.JuegoDAOJPAImpl;
import application.persistencia.LineaventaJPAImpl;
import application.persistencia.VentaDAOJPAImpl;
import application.util.DialogsUtil;
import application.util.ReportUtil;
import application.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ListChangeListener.Change;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

@FXMLController(value = "../vista/VistaVenta.fxml")
public class VistaVentaController {
	@FXML
	private TableView<Lineaventa> tabla;
	@FXML
	private TableColumn<Lineaventa, String> colJuego;
	@FXML
	private TableColumn<Lineaventa, String> colCantidad;
	@FXML
	private TableColumn<Lineaventa, String> colSubtotal;
	@FXML
	private ToolBar toolBar;
	@FXML
	private Pane panelIzq;
	@FXML
	@ActionTrigger("nuevoJuego")
	private Button botonAgregar;
	@FXML
	@ActionTrigger("editarJuego")
	private Button botonEditar;
	@FXML
	@ActionTrigger("borrarJuego")
	private Button botonEliminar;
	@FXML
	private Pane panelDer;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	private Button botonCancelar;
	@FXML
	private TextField textTotal;
	@FXML
	private TextField textMonto;
	@FXML
	private TextField textVuelto;
	@FXML
	private ComboBox<Cliente> comboCliente;

	private ObservableList<Lineaventa> itemsTabla;
	private ObservableList<Cliente> clientes;
	LineaventaJPAImpl daoLinea;
	VentaDAOJPAImpl daoVenta;
	ClienteDAOJPAImpl daoCliente;
	EmpleadoDAOJPAImpl daoEmpleado;


	@PostConstruct
	public void onInit() {
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		clientes = FXCollections.observableArrayList();
		daoVenta = new VentaDAOJPAImpl();
		daoLinea = new LineaventaJPAImpl();
		daoEmpleado = new EmpleadoDAOJPAImpl();
		daoCliente = new ClienteDAOJPAImpl();
		clientes.addAll(daoCliente.buscarTodos());
		comboCliente.setItems(clientes);
		tabla.setItems(itemsTabla);
		this.textTotal.setEditable(false);
		this.textVuelto.setEditable(false);
		//vaciarEspacios();
	}

	public void iniciarColumnas() {
		this.colJuego.setCellValueFactory(new PropertyValueFactory<>("juego"));

		this.colCantidad
				.setCellValueFactory(new PropertyValueFactory<>("cantidad"));

		this.colSubtotal.setCellValueFactory(new PropertyValueFactory<>(
				"subtotal"));

		tabla.getSelectionModel().getSelectedIndices()
		.addListener(new ListChangeListener<Integer>() {
			@Override
			public void onChanged(Change<? extends Integer> change) {
				if (change.getList().size() >= 1) {
					CalcularTotal();
					
				}
			}
		});
		
		this.textMonto.setOnKeyReleased(event -> {
			this.CalcularTotal();
		});
	}

	@ActionMethod("nuevoJuego")
	public void nuevoJuego() {
		Lineaventa lineaNueva = iniciarDialog(null);
		 if (lineaNueva != null) { 
			 itemsTabla.add(lineaNueva);
			 CalcularTotal();
		 } else {
			 System.out.println("nula");
		 }
		 
	}

	@ActionMethod("borrarJuego")
	public void borrarCliente() {
		if(Validate.tableNotSelectedItemValidate(tabla)) {
			Lineaventa linea = tabla.getSelectionModel().getSelectedItem();
			itemsTabla.remove(linea);
			CalcularTotal();
		}
	}

	@ActionMethod("editarJuego")
	public void editarCliente() {
		if(Validate.tableNotSelectedItemValidate(tabla)) {
			Lineaventa linea = tabla.getSelectionModel().getSelectedItem();
			linea = iniciarDialog(linea);
			CalcularTotal();
		}
	}


	public void vaciarEspacios() {
		//TODO imagen por defecto
	}


	public Lineaventa iniciarDialog(Lineaventa linea) {
		DialogLineaVentaController controladorDialog;
		Stage stage = new Stage();
		AnchorPane root;
		try {
			Flow mainFlow = new Flow(DialogLineaVentaController.class);
			FlowHandler mainFlowHandler = mainFlow.createHandler();
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			controladorDialog = (DialogLineaVentaController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			Scene scene = new Scene(pane);
			scene.getStylesheets().add("/application/application.css");
			stage.setScene(scene);
			stage.setTitle("Linea de Venta");
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setStage(stage);
			if (linea != null) {
				controladorDialog.setLineaventa(linea);
			}
			stage.showAndWait();
			return controladorDialog.getLineaventa();
			
		} catch (FlowException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	@ActionMethod("aceptar")
	public void aceptarVenta() {
		if (validar()) {
			Double total = 0D;
			Venta venta = new Venta();
			for (Lineaventa linea : itemsTabla) {
				venta.addLineaventa(linea);
				total += linea.getSubtotal().doubleValue();
			}
			venta.setCliente(comboCliente.getSelectionModel().getSelectedItem());
			venta.setEmpleado(VistaRootController.getEmpleadoActual());
			LocalDate fecha = LocalDate.now();
			venta.setFecha(fecha.toString());
			BigDecimal totalDecimal = new BigDecimal(total);
			venta.setTotal(totalDecimal);
			daoVenta.insertar(venta);
			itemsTabla.clear();
			
			if(DialogsUtil.confirmationDialog("Reporte", "Se va a imprimir el comprobante de venta", 
					"Presione OK para continuar") == true) {
				Map<String, Object> parametrosMap = new HashMap();
	    		parametrosMap.put( "id", new Integer(venta.getIdVenta()));	
	    		ReportUtil reporterReportUtil = new ReportUtil();
	    		reporterReportUtil.imprimirPDF("../reporte/venta.jasper", parametrosMap, 
	    				"reporte");
			}
		}
	}
	
	public void CalcularTotal() {
		try {
			Double total = 0D;
			for (Lineaventa linea : itemsTabla) {
				total += linea.getSubtotal().doubleValue();
			}
			this.textTotal.setText("" + total);
			Double monto = Double.parseDouble(this.textMonto.getText());
			Double vuelto = monto - total;
			textVuelto.setText("" + vuelto);
		} catch (NullPointerException e) {
			// TODO: handle exception
		} catch (NumberFormatException ex) {
			
		}
	}
	
	public boolean validar() {
		if(
			    Validate.comboValidate(comboCliente, "cliente")
			    && Validate.tableValidate(tabla, "tabla")
			    && Validate.textFieldDecimalValidate(textMonto, "monto")
				) {
			return true;
		}
		return false;
	}
	
}
