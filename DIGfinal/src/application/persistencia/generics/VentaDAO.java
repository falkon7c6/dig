package application.persistencia.generics;

import application.modelo.Venta;

public interface VentaDAO extends GenericDAO<Venta , Integer>{
	
}
