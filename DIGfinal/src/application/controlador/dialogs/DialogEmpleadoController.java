package application.controlador.dialogs;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;

import javax.annotation.PostConstruct;

import application.controlador.VistaEmpleadoController;
import application.modelo.Empleado;
import application.util.DialogsUtil;
import application.util.Seguridad;
import application.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;

@FXMLController(value = "../../vista/dialogs/DialogEmpleado.fxml")
public class DialogEmpleadoController {
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textApellido;
	@FXML
	private TextField textTelefono;
	@FXML
	private TextField textDireccion;
	@FXML
	private TextField textDni;
	@FXML
	private DatePicker dateFecha;
	@FXML
	private CheckBox checkHabilitado;
	@FXML
	private PasswordField textPass1;
	@FXML
	private PasswordField textPass2;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;

	private VistaEmpleadoController controladorVentanaPrincipal;
	private Empleado empleado;
	Stage stage;

	@PostConstruct
	public void init() {

	}

	@ActionMethod("aceptar")
	public void nuevoEmpleado() {
		if (validar() == true && validarPassword() == true) {
			if (empleado == null) {
				empleado = new Empleado();
			}
			empleado.setNombre(textNombre.getText());
			empleado.setApellido(textApellido.getText());
			empleado.setDireccion(textDireccion.getText());
			empleado.setDni(textDni.getText());
			// empleado.setHabilitado(1);
			empleado.setTelefono(textTelefono.getText());
			empleado.setFechaIngreso(this.dateFecha.getValue().toString());
			try {
				empleado.setPassword(Seguridad.SHA1(textPass1.getText()));
			} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			stage.close();
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		stage.close();
	}

	public boolean validar() {
		if (Validate.textFieldValidate(textApellido, "apellido")
				&& Validate.textFieldValidate(textDireccion, "direccion")
				&& Validate.textFieldValidate(textNombre, "nombre")
				&& Validate.textFieldNumberValidate(textTelefono, "telefono")
				&& Validate.ExpresionTextFieldValidate(textDni, "[0-9]{8}",
						"Dni", "8 numeros sin espacios")
				&& Validate.datePickerValidate(dateFecha, "fecha de Ingreso")) {
			return true;
		}
		return false;
	}

	public boolean validarPassword() {
		if (this.textPass1.getText().equals(this.textPass2.getText())) {
			return true;
		}
		DialogsUtil.errorDialog("Passwords", "error en el password",
				"los passwords ingresados no coinciden");
		return false;
	}

	public void llenarCampos(Empleado emp) {
		try {
			this.textApellido.setText(emp.getApellido());
			this.textDireccion.setText(emp.getDireccion());
			this.textDni.setText(emp.getDni());
			this.textNombre.setText(emp.getNombre());
			this.textTelefono.setText(emp.getTelefono());
			LocalDate fecha = LocalDate.parse(emp.getFechaIngreso());
			this.dateFecha.setValue(fecha);
		} catch (NullPointerException ex) {
			
		}
	}

	public Empleado getEmpleado() {
		return empleado;
	}

	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}

	public void setControladorVentanaPrincipal(
			VistaEmpleadoController controlador) {
		this.controladorVentanaPrincipal = controlador;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
