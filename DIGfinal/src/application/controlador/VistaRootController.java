package application.controlador;

import javax.annotation.PostConstruct;

import application.modelo.Empleado;
import application.util.DialogsUtil;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.util.VetoException;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.control.Menu;
import javafx.util.Duration;

@FXMLController(value = "../vista/VistaRoot.fxml")
public class VistaRootController {
	@FXML
	private ToolBar toolBarBot;
	@FXML
	private VBox vBoxTop;
	@FXML
	private MenuBar menuBar;
	@FXML
	private Menu menuArchivo;
	@FXML
	private MenuItem menuItemSalir;
	@FXML
	@ActionTrigger("saludar")
	private Menu menuAcercaDe;
	@FXML
	private MenuItem menuDatos;
	@FXML
	private ToolBar toolBarTOP;
	@FXML
	@ActionTrigger("aCliente")
	private Button botonCliente;
	@FXML
	@ActionTrigger("aEmpleado")
	private Button botonEmpleado;
	@FXML
	@ActionTrigger("aVenta")
	private Button botonPedido;
	@FXML
	@ActionTrigger("aJuego")
	private Button botonProductos;
	@FXML
	private StackPane centro;


	private Flow innerFlow;
	private FlowHandler flowHandler;
	private static Empleado empleadoActual;

	@PostConstruct
	public void onInit() {
		iniciarCentro();
		iniciarMenu();
		System.out.println(empleadoActual.toString());
	}

	public void iniciarCentro() {
		try {
			innerFlow = new Flow(VistaClienteController.class);
			flowHandler = innerFlow.createHandler();
			centro.getChildren().add(
					flowHandler.start(new DefaultFlowContainer()));
		} catch (FlowException e) {
			e.printStackTrace();
		}
	}

	public void iniciarMenu() {
		this.menuItemSalir.setOnAction(event -> {
			System.exit(0);
		});
	}

	@ActionMethod("saludar")
	public void saludar() {
		DialogsUtil.messageDialog("Datos:",
				" Alumno: Ortiz Bulacios, Luis Gabriel" + "\n Legajo: 33086"
						+ "\n Regular: 2012"
						+ "\n Final Dise�o de Interfaz Grafica");
	}

	@ActionMethod("aCliente")
	public void aCliente() {
		try {
			flowHandler.navigateTo(VistaClienteController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("aEmpleado")
	public void aEmpleado() {
		try {
			flowHandler.navigateTo(VistaEmpleadoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@ActionMethod("aJuego")
	public void aJuego() {
		try {
			flowHandler.navigateTo(VistaJuegoController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	
	@ActionMethod("aVenta")
	public void aVenta() {
		try {
			flowHandler.navigateTo(VistaVentaController.class);
		} catch (VetoException | FlowException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public static Empleado getEmpleadoActual() {
		return empleadoActual;
	}

	public static void setEmpleadoActual(Empleado empleadoActual) {
		VistaRootController.empleadoActual = empleadoActual;
	}

	
}
