package application.controlador;

import java.io.IOException;
import java.time.LocalDate;

import javax.annotation.PostConstruct;

import application.Main;
import application.controlador.dialogs.DialogEmpleadoController;
import application.modelo.Empleado;
import application.persistencia.EmpleadoDAOJPAImpl;
import application.util.DialogsUtil;
import application.util.Validate;
import io.datafx.controller.FXMLController;
import io.datafx.controller.context.ViewContext;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

@FXMLController(value = "../vista/VistaEmpleado.fxml")
public class VistaEmpleadoController {
	@FXML
	private TableView<Empleado> tabla;
	@FXML
	private TableColumn<Empleado, String> colLegajo;
	@FXML
	private TableColumn<Empleado, String> colNombre;
	@FXML
	private TableColumn<Empleado, String> colApellido;
	@FXML
	private TableColumn<Empleado, String> colTelefono;
	@FXML
	private TableColumn<Empleado, String> colDireccion;
	@FXML
	private TableColumn<Empleado, String> colFechaIngreso;
	@FXML
	private TableColumn<Empleado, String> colDni;
	@FXML
	private TableColumn<Empleado, String> colHabilitado;
	@FXML
	private ToolBar toolBar;
	@FXML
	private Pane panelIzq;
	private Main main;

	@FXML
	@ActionTrigger("nuevoEmpleado")
	private Button botonNuevo;

	@FXML
	@ActionTrigger("editarEmpleado")
	private Button botonEditar;
	@FXML
	@ActionTrigger("borrarEmpleado")
	private Button botonEliminar;

	@FXML
	private Pane panelDer;

	private ObservableList<Empleado> itemsTabla;
	private EmpleadoDAOJPAImpl dao;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	@PostConstruct
	public void onInit() {
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		dao = new EmpleadoDAOJPAImpl();
		itemsTabla.setAll(dao.buscarTodos());
		tabla.setItems(itemsTabla);
	}

	public void iniciarColumnas() {
		this.colLegajo
				.setCellValueFactory(new PropertyValueFactory<>("legajo"));

		this.colApellido.setCellValueFactory(new PropertyValueFactory<>(
				"apellido"));

		this.colDireccion.setCellValueFactory(new PropertyValueFactory<>(
				"direccion"));

		this.colDni.setCellValueFactory(new PropertyValueFactory<>("dni"));

		this.colFechaIngreso.setCellValueFactory(new PropertyValueFactory<>(
				"fechaIngreso"));

		this.colHabilitado.setCellValueFactory(new PropertyValueFactory<>(
				"habilitado"));

		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));

		this.colTelefono.setCellValueFactory(new PropertyValueFactory<>(
				"telefono"));

	}

	@ActionMethod("nuevoEmpleado")
	public void nuevoEmpleado() {
		Empleado empleado = iniciarDialog(null);
		if (empleado != null) {
			dao.insertar(empleado);
			itemsTabla.add(empleado);
		}

	}

	@ActionMethod("borrarEmpleado")
	public void borrarEmpleado() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			if (this.itemsTabla.size() > 1) {
				Empleado temp = tabla.getSelectionModel().getSelectedItem();
				dao.borrar(temp);
				itemsTabla.remove(temp);
			} else {
				DialogsUtil.errorDialog("Error", "Error al eliminar empleado",
						"No puede quedarse sin empleados en el sistema");
			}
		}
	}

	@ActionMethod("editarEmpleado")
	public void editarEmpleado() {
		if (Validate.tableNotSelectedItemValidate(tabla)) {
			Empleado temp = tabla.getSelectionModel().getSelectedItem();
			temp = iniciarDialog(temp);
			dao.salvar(temp);
		}
	}

	public Empleado iniciarDialog(Empleado emp) {
		DialogEmpleadoController controladorDialog;
		Stage stage = new Stage();
		AnchorPane root;
		try {
			Flow mainFlow = new Flow(DialogEmpleadoController.class);
			FlowHandler mainFlowHandler = mainFlow.createHandler();
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			controladorDialog = (DialogEmpleadoController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			Scene scene = new Scene(pane);
			scene.getStylesheets().add("/application/application.css");
			stage.setScene(scene);
			stage.setTitle("Empleado");
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setStage(stage);
			if (emp != null) {
				controladorDialog.setEmpleado(emp);
				controladorDialog.llenarCampos(emp);
			}
			stage.showAndWait();
			return controladorDialog.getEmpleado();
		} catch (FlowException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setMain(Main main) {
		this.main = main;
	}
}
