package application.modelo;

import java.io.Serializable;
import java.math.BigDecimal;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.*;

import java.util.Date;
import java.util.List;


/**
 * The persistent class for the juego database table.
 * 
 */
@Entity
@NamedQuery(name="Juego.findAll", query="SELECT j FROM Juego j")
public class Juego implements Serializable {
	private static final long serialVersionUID = 1L;
	private SimpleIntegerProperty idJuego = new SimpleIntegerProperty();
	private SimpleStringProperty desarrollador = new SimpleStringProperty();
	private SimpleStringProperty descripcion = new SimpleStringProperty();
	private SimpleStringProperty distribuidor = new SimpleStringProperty();
	private SimpleStringProperty fecha= new SimpleStringProperty();
	private SimpleStringProperty genero = new SimpleStringProperty();
	private byte[] imagen;
	private SimpleStringProperty nombre = new SimpleStringProperty();
	private SimpleStringProperty plataforma = new SimpleStringProperty();
	private SimpleIntegerProperty stock = new SimpleIntegerProperty();
	private List<Lineaventa> lineapedidos;
	private SimpleObjectProperty<BigDecimal> precio = new SimpleObjectProperty<BigDecimal>();

	public Juego() {
	}

	
	
	@Override
	public String toString() {
		return nombre.get() + ", " + plataforma.get() + ", " + desarrollador.get() + ", " + getGenero();
	}



	//bi-directional many-to-one association to Lineapedido
	@OneToMany(mappedBy="juego", cascade = CascadeType.ALL)
	public List<Lineaventa> getLineapedidos() {
		return this.lineapedidos;
	}

	public void setLineapedidos(List<Lineaventa> lineapedidos) {
		this.lineapedidos = lineapedidos;
	}

	public Lineaventa addLineapedido(Lineaventa lineapedido) {
		getLineapedidos().add(lineapedido);
		lineapedido.setJuego(this);

		return lineapedido;
	}

	public Lineaventa removeLineapedido(Lineaventa lineapedido) {
		getLineapedidos().remove(lineapedido);
		lineapedido.setJuego(null);

		return lineapedido;
	}



	public final SimpleStringProperty desarrolladorProperty() {
		return this.desarrollador;
	}



	public final java.lang.String getDesarrollador() {
		return this.desarrolladorProperty().get();
	}



	public final void setDesarrollador(final java.lang.String desarrollador) {
		this.desarrolladorProperty().set(desarrollador);
	}



	public final SimpleStringProperty descripcionProperty() {
		return this.descripcion;
	}



	public final java.lang.String getDescripcion() {
		return this.descripcionProperty().get();
	}



	public final void setDescripcion(final java.lang.String descripcion) {
		this.descripcionProperty().set(descripcion);
	}



	public final SimpleStringProperty distribuidorProperty() {
		return this.distribuidor;
	}



	public final java.lang.String getDistribuidor() {
		return this.distribuidorProperty().get();
	}



	public final void setDistribuidor(final java.lang.String distribuidor) {
		this.distribuidorProperty().set(distribuidor);
	}


	public final SimpleStringProperty generoProperty() {
		return this.genero;
	}



	public final java.lang.String getGenero() {
		return this.generoProperty().get();
	}



	public final void setGenero(final java.lang.String genero) {
		this.generoProperty().set(genero);
	}


	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}



	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}



	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}



	public final SimpleStringProperty plataformaProperty() {
		return this.plataforma;
	}



	public final java.lang.String getPlataforma() {
		return this.plataformaProperty().get();
	}



	public final void setPlataforma(final java.lang.String plataforma) {
		this.plataformaProperty().set(plataforma);
	}



	public final SimpleIntegerProperty stockProperty() {
		return this.stock;
	}



	public final int getStock() {
		return this.stockProperty().get();
	}



	public final void setStock(final int stock) {
		this.stockProperty().set(stock);
	}



	public final SimpleIntegerProperty idJuegoProperty() {
		return this.idJuego;
	}


	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	public final int getIdJuego() {
		return this.idJuegoProperty().get();
	}



	public final void setIdJuego(final int idJuego) {
		this.idJuegoProperty().set(idJuego);
	}

	
	@Lob
	@Basic(fetch = FetchType.LAZY)
	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	public final SimpleStringProperty fechaProperty() {
		return this.fecha;
	}

	public final java.lang.String getFecha() {
		return this.fechaProperty().get();
	}

	public final void setFecha(final java.lang.String fecha) {
		this.fechaProperty().set(fecha);
	}



	public final SimpleObjectProperty<BigDecimal> precioProperty() {
		return this.precio;
	}



	public final java.math.BigDecimal getPrecio() {
		return this.precioProperty().get();
	}



	public final void setPrecio(final java.math.BigDecimal precio) {
		this.precioProperty().set(precio);
	}

	
}