package application.modelo;

import java.io.Serializable;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import javax.persistence.*;

import java.math.BigDecimal;

/**
 * The persistent class for the lineapedido database table.
 * 
 */
@Entity
@NamedQuery(name = "Lineaventa.findAll", query = "SELECT l FROM Lineaventa l")
public class Lineaventa implements Serializable {
	private static final long serialVersionUID = 1L;
	private SimpleIntegerProperty idlineaVenta = new SimpleIntegerProperty();
	private SimpleIntegerProperty cantidad = new SimpleIntegerProperty();
	private SimpleObjectProperty<BigDecimal> subtotal = new SimpleObjectProperty<BigDecimal>();
	private SimpleObjectProperty<Juego> juego = new SimpleObjectProperty<Juego>();
	private SimpleObjectProperty<Venta> venta = new SimpleObjectProperty<Venta>();

	public Lineaventa() {
	}
	
	public void calcularSubTotal() {
		Double subtotalTemp = 0D;
		subtotalTemp = cantidad.get() * juego.get().getPrecio().doubleValue();
		subtotal.set(new BigDecimal(subtotalTemp));
	}

	public final SimpleIntegerProperty cantidadProperty() {
		return this.cantidad;
	}

	public final int getCantidad() {
		return this.cantidadProperty().get();
	}

	public final void setCantidad(final int cantidad) {
		this.cantidadProperty().set(cantidad);
	}

	public final SimpleObjectProperty<BigDecimal> subtotalProperty() {
		return this.subtotal;
	}

	public final java.math.BigDecimal getSubtotal() {
		return this.subtotalProperty().get();
	}

	public final void setSubtotal(final java.math.BigDecimal subtotal) {
		this.subtotalProperty().set(subtotal);
	}

	public final SimpleObjectProperty<Juego> juegoProperty() {
		return this.juego;
	}

	// bi-directional many-to-one association to Juego
	@ManyToOne
	public final application.modelo.Juego getJuego() {
		return this.juegoProperty().get();
	}

	public final void setJuego(final application.modelo.Juego juego) {
		this.juegoProperty().set(juego);
	}

	public final SimpleIntegerProperty idlineaVentaProperty() {
		return this.idlineaVenta;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public final int getIdlineaVenta() {
		return this.idlineaVentaProperty().get();
	}

	public final void setIdlineaVenta(final int idlineaVenta) {
		this.idlineaVentaProperty().set(idlineaVenta);
	}

	public final SimpleObjectProperty<Venta> ventaProperty() {
		return this.venta;
	}

	// bi-directional many-to-one association to Pedido
	@ManyToOne
	public final application.modelo.Venta getVenta() {
		return this.ventaProperty().get();
	}

	public final void setVenta(final application.modelo.Venta venta) {
		this.ventaProperty().set(venta);
	}

	/*
	 * public int getIdlineaPedido() { return this.idlineaPedido; }
	 * 
	 * public void setIdlineaPedido(int idlineaPedido) { this.idlineaPedido =
	 * idlineaPedido; }
	 * 
	 * 
	 * public int getCantidad() { return this.cantidad; }
	 * 
	 * public void setCantidad(int cantidad) { this.cantidad = cantidad; }
	 * 
	 * 
	 * public BigDecimal getSubtotal() { return this.subtotal; }
	 * 
	 * public void setSubtotal(BigDecimal subtotal) { this.subtotal = subtotal;
	 * }
	 * 
	 * 
	 * 
	 * public Juego getJuego() { return this.juego; }
	 * 
	 * public void setJuego(Juego juego) { this.juego = juego; }
	 * 
	 * 
	 * 
	 * public Pedido getPedido() { return this.pedido; }
	 * 
	 * public void setPedido(Pedido pedido) { this.pedido = pedido; }
	 */

}