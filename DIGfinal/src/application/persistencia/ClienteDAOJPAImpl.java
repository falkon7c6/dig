package application.persistencia;

import application.modelo.Cliente;
import application.persistencia.generics.ClienteDAO;
import application.persistencia.generics.GenericDAOJPAImpl;

public class ClienteDAOJPAImpl extends GenericDAOJPAImpl<Cliente, Integer> implements ClienteDAO{

}
