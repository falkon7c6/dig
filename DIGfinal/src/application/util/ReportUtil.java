package application.util;

import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import com.mysql.jdbc.Connection;

import application.persistencia.generics.JPAHelper;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.fill.ReportFiller;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class ReportUtil {

	private JasperReport reporte;
	private JasperPrint reportFilled;
	private JasperViewer visor;

	public ReportUtil() {

	}

	public void imprimirPDF(String path, Map parametros, String nombre) {
		try {
			
			
			EntityManagerFactory factoriaSession = JPAHelper.getJPAFactory();
			EntityManager manager = factoriaSession.createEntityManager();
			manager.getTransaction().begin();
			Connection conexion =  (Connection) manager.unwrap(java.sql.Connection.class);
			URL in = this.getClass().getResource(path);
			reporte = (JasperReport) JRLoader.loadObject(in);
			
			reportFilled = JasperFillManager.fillReport(reporte, parametros,
					conexion);
			visor = new JasperViewer(reportFilled, false);
			visor.setVisible(true);
			manager.getTransaction().commit();
			manager.close();
			//manager.close();
		} catch (JRException e) {
			e.printStackTrace();
		} catch (NullPointerException ex) {
			ex.printStackTrace();
		}
		
	}
}
