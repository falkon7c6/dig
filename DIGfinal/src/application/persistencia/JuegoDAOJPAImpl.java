package application.persistencia;


import application.modelo.Juego;
import application.persistencia.generics.GenericDAOJPAImpl;
import application.persistencia.generics.JuegoDAO;

public class JuegoDAOJPAImpl extends GenericDAOJPAImpl<Juego, Integer> implements JuegoDAO{

}
