package application.controlador;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.flow.container.AnimatedFlowContainer;
import io.datafx.controller.flow.container.ContainerAnimations;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;

import java.io.IOException;

import javax.annotation.PostConstruct;

import application.controlador.dialogs.DialogClienteController;
import application.modelo.Cliente;
import application.persistencia.ClienteDAOJPAImpl;
import application.util.Validate;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

@FXMLController(value = "../vista/VistaCliente.fxml")
public class VistaClienteController {
	@FXML
	private TableView<Cliente> tabla;
	@FXML
	private TableColumn<Cliente, String> colID;
	@FXML
	private TableColumn<Cliente, String> colNombre;
	@FXML
	private TableColumn<Cliente, String> colApellido;
	@FXML
	private TableColumn<Cliente, String> colCUILCUIT;
	@FXML
	private TableColumn<Cliente, String> colTelefono;
	@FXML
	private TableColumn<Cliente, String> colDireccion;
	@FXML
	private TableColumn<Cliente, String> colMail;
	@FXML
	private TableColumn<Cliente, String> colTipo;
	@FXML
	private ToolBar toolBar;
	@FXML
	private Pane panelIzq;
	@FXML
	@ActionTrigger("nuevoCliente")
	private Button botonNuevo;
	@FXML
	@ActionTrigger("editarCliente")
	private Button botonEditar;
	@FXML
	@ActionTrigger("borrarCliente")
	private Button botonEliminar;
	@FXML
	private Pane panelDer;

	private ObservableList<Cliente> itemsTabla;
	private ClienteDAOJPAImpl dao;

	@FXMLViewFlowContext
	private ViewFlowContext context;

	@PostConstruct
	public void onInit() {
		iniciarColumnas();
		itemsTabla = FXCollections.observableArrayList();
		dao = new ClienteDAOJPAImpl();
		itemsTabla.setAll(dao.buscarTodos());
		tabla.setItems(itemsTabla);

		
	}

	public void iniciarColumnas() {
		this.colID.setCellValueFactory(new PropertyValueFactory<>("idCliete"));

		this.colApellido.setCellValueFactory(new PropertyValueFactory<>(
				"apellido"));

		this.colDireccion.setCellValueFactory(new PropertyValueFactory<>(
				"direccion"));

		this.colCUILCUIT.setCellValueFactory(new PropertyValueFactory<>(
				"cuilCuit"));

		this.colMail.setCellValueFactory(new PropertyValueFactory<>("mail"));

		this.colTipo.setCellValueFactory(new PropertyValueFactory<>("tipo"));

		this.colNombre
				.setCellValueFactory(new PropertyValueFactory<>("nombre"));

		this.colTelefono.setCellValueFactory(new PropertyValueFactory<>(
				"telefono"));

	}

	@ActionMethod("nuevoCliente")
	public void nuevoCliente() {
		Cliente cliente = iniciarDialog(null);
		if (cliente != null) {
			dao.insertar(cliente);
			itemsTabla.add(cliente);
		}

	}

	@ActionMethod("borrarCliente")
	public void borrarCliente() {
		if(Validate.tableNotSelectedItemValidate(tabla)) {
			Cliente temp = tabla.getSelectionModel().getSelectedItem();
			dao.borrar(temp);
			itemsTabla.remove(temp);
		}
		
	}

	@ActionMethod("editarCliente")
	public void editarCliente() {
		if(Validate.tableNotSelectedItemValidate(tabla)) {
			Cliente temp = tabla.getSelectionModel().getSelectedItem();
		temp = iniciarDialog(temp);
		dao.salvar(temp);
		}
		
	}

	public Cliente iniciarDialog(Cliente cli) {
		DialogClienteController controladorDialog;
		Stage stage = new Stage();
		AnchorPane root;
		try {
			Flow mainFlow = new Flow(DialogClienteController.class);
			FlowHandler mainFlowHandler = mainFlow.createHandler();
			StackPane pane = mainFlowHandler.start(new AnimatedFlowContainer(
					Duration.millis(320), ContainerAnimations.ZOOM_IN));
			controladorDialog = (DialogClienteController) mainFlowHandler
					.getCurrentViewContext().getController();
			controladorDialog.setControladorVentanaPrincipal(this);
			Scene scene = new Scene(pane);
			scene.getStylesheets().add("/application/application.css");
			stage.setScene(scene);
			stage.setTitle("Cliente");
			stage.initModality(Modality.APPLICATION_MODAL);
			controladorDialog.setStage(stage);
			if (cli != null) {
				controladorDialog.setCliente(cli);
				controladorDialog.llenarCampos(cli);
			}
			stage.showAndWait();
			return controladorDialog.getCliente();
		} catch (FlowException e) {
			e.printStackTrace();
		}

		return null;
	}
}
