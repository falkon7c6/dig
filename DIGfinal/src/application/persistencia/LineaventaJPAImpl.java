package application.persistencia;

import application.modelo.Lineaventa;
import application.persistencia.generics.GenericDAOJPAImpl;
import application.persistencia.generics.LineaventaDAO;

public class LineaventaJPAImpl extends GenericDAOJPAImpl<Lineaventa, Integer> implements LineaventaDAO{

}
