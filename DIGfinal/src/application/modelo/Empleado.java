package application.modelo;

import java.io.Serializable;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;

import javax.persistence.*;

import java.util.Date;
import java.util.List;

/**
 * The persistent class for the empleado database table.
 * 
 */
@Entity
@NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e")
public class Empleado implements Serializable {
	private static final long serialVersionUID = 1L;
	private SimpleIntegerProperty legajo = new SimpleIntegerProperty();
	private SimpleStringProperty apellido = new SimpleStringProperty();
	private SimpleStringProperty direccion = new SimpleStringProperty();
	private SimpleStringProperty dni = new SimpleStringProperty();
	private SimpleStringProperty fechaIngreso = new SimpleStringProperty();
	private SimpleStringProperty password = new SimpleStringProperty();
	//private SimpleIntegerProperty habilitado = new SimpleIntegerProperty();;
	private SimpleStringProperty nombre = new SimpleStringProperty();
	private SimpleStringProperty telefono = new SimpleStringProperty();
	private List<Venta> pedidos;

	public Empleado() {
	}

	// ///////////////////
	
	

	public final SimpleIntegerProperty legajoProperty() {
		return this.legajo;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public final int getLegajo() {
		return this.legajoProperty().get();
	}

	public final void setLegajo(final int legajo) {
		this.legajoProperty().set(legajo);
	}

	public final SimpleStringProperty apellidoProperty() {
		return this.apellido;
	}

	public final java.lang.String getApellido() {
		return this.apellidoProperty().get();
	}

	public final void setApellido(final java.lang.String apellido) {
		this.apellidoProperty().set(apellido);
	}

	public final SimpleStringProperty direccionProperty() {
		return this.direccion;
	}

	public final java.lang.String getDireccion() {
		return this.direccionProperty().get();
	}

	public final void setDireccion(final java.lang.String direccion) {
		this.direccionProperty().set(direccion);
	}

	public final SimpleStringProperty dniProperty() {
		return this.dni;
	}

	public final java.lang.String getDni() {
		return this.dniProperty().get();
	}

	public final void setDni(final java.lang.String dni) {
		this.dniProperty().set(dni);
	}

	

	/*public final SimpleIntegerProperty habilitadoProperty() {
		return this.habilitado;
	}

	public final int getHabilitado() {
		return this.habilitadoProperty().get();
	}

	public final void setHabilitado(final int habilitado) {
		this.habilitadoProperty().set(habilitado);
	}*/

	public final SimpleStringProperty nombreProperty() {
		return this.nombre;
	}

	public final java.lang.String getNombre() {
		return this.nombreProperty().get();
	}

	public final void setNombre(final java.lang.String nombre) {
		this.nombreProperty().set(nombre);
	}

	public final SimpleStringProperty telefonoProperty() {
		return this.telefono;
	}

	public final java.lang.String getTelefono() {
		return this.telefonoProperty().get();
	}

	public final void setTelefono(final java.lang.String telefono) {
		this.telefonoProperty().set(telefono);
	}

	// bi-directional many-to-one association to Pedido

	@OneToMany(mappedBy = "empleado", cascade = CascadeType.ALL)
	public List<Venta> getPedidos() {
		return this.pedidos;
	}

	public void setPedidos(List<Venta> pedidos) {
		this.pedidos = pedidos;
	}

	public Venta addPedido(Venta pedido) {
		getPedidos().add(pedido);
		pedido.setEmpleado(this);

		return pedido;
	}

	public Venta removePedido(Venta pedido) {
		getPedidos().remove(pedido);
		pedido.setEmpleado(null);

		return pedido;
	}

	public final SimpleStringProperty fechaIngresoProperty() {
		return this.fechaIngreso;
	}

	public final java.lang.String getFechaIngreso() {
		return this.fechaIngresoProperty().get();
	}

	public final void setFechaIngreso(final java.lang.String fechaIngreso) {
		this.fechaIngresoProperty().set(fechaIngreso);
	}

	public final SimpleStringProperty passwordProperty() {
		return this.password;
	}

	public final java.lang.String getPassword() {
		return this.passwordProperty().get();
	}

	public final void setPassword(final java.lang.String password) {
		this.passwordProperty().set(password);
	}

	// ///////////////

	/*
	 * @Id
	 * 
	 * @GeneratedValue(strategy=GenerationType.IDENTITY) public int getLegajo()
	 * { return this.legajo; }
	 * 
	 * public void setLegajo(int legajo) { this.legajo = legajo; }
	 * 
	 * 
	 * public String getApellido() { return this.apellido; }
	 * 
	 * public void setApellido(String apellido) { this.apellido = apellido; }
	 * 
	 * 
	 * public String getDireccion() { return this.direccion; }
	 * 
	 * public void setDireccion(String direccion) { this.direccion = direccion;
	 * }
	 * 
	 * 
	 * public String getDni() { return this.dni; }
	 * 
	 * public void setDni(String dni) { this.dni = dni; }
	 * 
	 * 
	 * @Temporal(TemporalType.DATE) public Date getFechaIngreso() { return
	 * this.fechaIngreso; }
	 * 
	 * public void setFechaIngreso(Date fechaIngreso) { this.fechaIngreso =
	 * fechaIngreso; }
	 * 
	 * 
	 * public byte getHabilitado() { return this.habilitado; }
	 * 
	 * public void setHabilitado(byte habilitado) { this.habilitado =
	 * habilitado; }
	 * 
	 * 
	 * public String getNombre() { return this.nombre; }
	 * 
	 * public void setNombre(String nombre) { this.nombre = nombre; }
	 * 
	 * 
	 * public String getTelefono() { return this.telefono; }
	 * 
	 * public void setTelefono(String telefono) { this.telefono = telefono; }
	 * 
	 * 
	 * //bi-directional many-to-one association to Pedido
	 * 
	 * @OneToMany(mappedBy="empleado") public List<Pedido> getPedidos() { return
	 * this.pedidos; }
	 * 
	 * public void setPedidos(List<Pedido> pedidos) { this.pedidos = pedidos; }
	 * 
	 * public Pedido addPedido(Pedido pedido) { getPedidos().add(pedido);
	 * pedido.setEmpleado(this);
	 * 
	 * return pedido; }
	 * 
	 * public Pedido removePedido(Pedido pedido) { getPedidos().remove(pedido);
	 * pedido.setEmpleado(null);
	 * 
	 * return pedido; }
	 */

}