package application.controlador.dialogs;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;

import javax.annotation.PostConstruct;



import javax.imageio.ImageIO;

import application.controlador.VistaJuegoController;
import application.modelo.Juego;
import application.util.DialogsUtil;
import application.util.Validate;
import application.util.archivoUtil;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ComboBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.DatePicker;
import javafx.stage.Stage;

@FXMLController(value = "../../vista/dialogs/DialogJuego.fxml")
public class DialogJuegoController {
	@FXML
	private TextField textNombre;
	@FXML
	private TextField textDescripcion;
	@FXML
	private TextField textGenero;
	@FXML
	private TextField textDesarrollador;
	@FXML
	private TextField textStock;
	@FXML
	private DatePicker dateFecha;
	@FXML
	private TextField textDistribuidor;
	@FXML
    private TextField textPrecio;
	@FXML
	@ActionTrigger("cargar")
	private Button botonCargarImagen;
	@FXML
	private ComboBox<String> comboPlataforma;
	@FXML
	@ActionTrigger("cancelar")
	private Button botonCancelar;
	@FXML
	@ActionTrigger("aceptar")
	private Button botonAceptar;
	@FXML
	private ImageView imagenCaratula;

	
	private VistaJuegoController controladorVentanaPrincipal;
	private Juego juego;
	private ObservableList<String> plataformasList;
	private byte[] caratula;
	Stage stage;
	
	@PostConstruct
	public void init() {
		plataformasList = FXCollections.observableArrayList();
		plataformasList.addAll("PC","Playstation","Xbox One","WiiU","Android","IOS");
		comboPlataforma.setItems(plataformasList);
	}
	
	@ActionMethod("aceptar")
	public void nuevoJuego() {
		if (validar()) {
			if(juego == null) {
				juego = new Juego();
			}
			juego.setNombre(textNombre.getText());
			juego.setDesarrollador(textDesarrollador.getText());
			juego.setDescripcion(textDescripcion.getText());
			juego.setDistribuidor(textDistribuidor.getText());
			juego.setFecha(dateFecha.getValue().toString());
			juego.setGenero(textGenero.getText());
			juego.setImagen(caratula);
			juego.setPlataforma(comboPlataforma.getSelectionModel().getSelectedItem());
			juego.setStock(Integer.valueOf(textStock.getText()));
			juego.setPrecio(new BigDecimal(textPrecio.getText()));
			stage.close();
		}
	}
	
	@ActionMethod("cargar")
	public void cargarImagen() {
		try {
			archivoUtil archivoUtil = new archivoUtil();
			File file = archivoUtil.cargarArchivo();
			if(file.length() < 3000000 && file != null) {
				//System.out.println("se va a romper " + file.length());
				try {
					BufferedImage bufferedImage = ImageIO.read(file);
					Image image = SwingFXUtils.toFXImage(bufferedImage, null);
					this.imagenCaratula.setImage(image);
					byte[] bFile = new byte[(int) file.length()];
					try {
						FileInputStream fileInputStream = new FileInputStream(file);
						fileInputStream.read(bFile);
						fileInputStream.close();
						caratula = bFile;
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (IOException ex) {
					ex.printStackTrace();
				} catch(NullPointerException ex) {
					System.out.println("null a controlar");
				}
			} else{
				DialogsUtil.messageDialog("Imagen", "la imagen del juego supera el tama�o maximo (2.86MB)");
			}
		} catch(Exception ex) {
			
		}
		
		
	}
	
	public boolean validar() {
		if(
				Validate.textFieldValidate(textDesarrollador, "Desarrollador") &&
				Validate.textFieldValidate(textDescripcion, "Descripcion") &&
				Validate.textFieldValidate(textDistribuidor, "Distribuidor") &&
				Validate.textFieldValidate(textGenero, "Genero") &&
				Validate.textFieldValidate(textNombre, "Nombre") &&
				Validate.comboValidate(comboPlataforma, "Plataforma") &&
				Validate.textFieldNumberValidate(textStock, "Stock") &&
				Validate.datePickerValidate(dateFecha, "Fecha de salida") &&
				Validate.textFieldDecimalValidate(textPrecio, "precio")
				) {
			return true;
		}
		return false;
	}
	
	public void llenarCampos(Juego jug) {
		try {
			this.textDesarrollador.setText(jug.getDesarrollador());
			this.textDescripcion.setText(jug.getDescripcion());
			this.textDistribuidor.setText(jug.getDistribuidor());
			this.textGenero.setText(jug.getGenero());
			this.textNombre.setText(jug.getNombre());
			this.textPrecio.setText("" + jug.getPrecio());
			this.textStock.setText("" + jug.getStock());
			this.comboPlataforma.getSelectionModel().select(jug.getPlataforma());
			LocalDate fecha = LocalDate.parse(jug.getFecha());
			this.dateFecha.setValue(fecha);
			cargarImagen(jug);
		} catch (NullPointerException e) {
			
		}
	}
	
	public void cargarImagen(Juego juego) {
		BufferedImage img;
		try {
			img = ImageIO.read(new ByteArrayInputStream(juego.getImagen()));
			Image image = SwingFXUtils.toFXImage(img, null);
			this.imagenCaratula.setImage(image);
		} catch (IOException e) {
			e.printStackTrace();
		} catch(NullPointerException ex) {
			System.out.println("sin imagen");
		}
	}

	@ActionMethod("cancelar")
	public void cancelar() {
		stage.close();
	}
	
	public Juego getJuego() {
		return juego;
	}

	public void setJuego(Juego juego) {
		this.juego = juego;
	}

	public void setControladorVentanaPrincipal(VistaJuegoController controlador) {
		this.controladorVentanaPrincipal = controlador;
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
}
