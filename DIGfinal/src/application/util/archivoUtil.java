package application.util;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javafx.stage.FileChooser;
import javafx.stage.Stage;



public class archivoUtil {

	private Desktop desktop = Desktop.getDesktop();
	
	public archivoUtil() {
		
		
	}
	
	public File cargarArchivo() {
		Stage newStage = new Stage();
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Open Resource File");
		fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png")
            );
		File file = fileChooser.showOpenDialog(newStage);
		if (file != null) {
            return file;
        }
		return null;
	}
	
	private void openFile(File file) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
           
        }
    }
}
