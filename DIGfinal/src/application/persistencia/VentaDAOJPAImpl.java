package application.persistencia;

import application.modelo.Venta;
import application.persistencia.generics.GenericDAOJPAImpl;
import application.persistencia.generics.VentaDAO;

public class VentaDAOJPAImpl extends GenericDAOJPAImpl<Venta, Integer> implements VentaDAO{

}
