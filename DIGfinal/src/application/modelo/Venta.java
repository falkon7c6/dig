package application.modelo;

import java.io.Serializable;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The persistent class for the pedido database table.
 * 
 */
@Entity
@NamedQuery(name = "Venta.findAll", query = "SELECT v FROM Venta v")
public class Venta implements Serializable {
	private static final long serialVersionUID = 1L;
	private SimpleIntegerProperty idVenta = new SimpleIntegerProperty();
	private SimpleStringProperty  fecha = new SimpleStringProperty();
	private SimpleObjectProperty<BigDecimal> total = new SimpleObjectProperty<BigDecimal>();
	private List<Lineaventa> lineaventas;
	private SimpleObjectProperty<Cliente> cliente = new SimpleObjectProperty<Cliente>();
	private SimpleObjectProperty<Empleado> empleado = new SimpleObjectProperty<Empleado>();

	public Venta() {
		lineaventas = new ArrayList<Lineaventa>();
	}
	
	public void calcularTotalVenta() {
		Double totalTemp = 0D;
		for (Lineaventa lineaventa : lineaventas) {
			totalTemp = lineaventa.getSubtotal().doubleValue();
		}
		total.setValue(new BigDecimal(totalTemp));
	}

	public final SimpleIntegerProperty idVentaProperty() {
		return this.idVenta;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public final int getIdVenta() {
		return this.idVentaProperty().get();
	}


	public final void setIdVenta(final int idVenta) {
		this.idVentaProperty().set(idVenta);
	}


	public final SimpleObjectProperty<BigDecimal> totalProperty() {
		return this.total;
	}

	public final java.math.BigDecimal getTotal() {
		return this.totalProperty().get();
	}

	public final void setTotal(final java.math.BigDecimal total) {
		this.totalProperty().set(total);
	}

	public final SimpleObjectProperty<Cliente> clienteProperty() {
		return this.cliente;
	}

	// bi-directional many-to-one association to Cliente
	@ManyToOne()
	public final application.modelo.Cliente getCliente() {
		return this.clienteProperty().get();
	}

	public final void setCliente(final application.modelo.Cliente cliente) {
		this.clienteProperty().set(cliente);
	}

	public final SimpleObjectProperty<Empleado> empleadoProperty() {
		return this.empleado;
	}

	// bi-directional many-to-one association to Empleado
	@ManyToOne
	public final application.modelo.Empleado getEmpleado() {
		return this.empleadoProperty().get();
	}

	public final void setEmpleado(final application.modelo.Empleado empleado) {
		this.empleadoProperty().set(empleado);
	}

	// bi-directional many-to-one association to Lineapedido
	@OneToMany(mappedBy = "venta", cascade = CascadeType.ALL)
	public List<Lineaventa> getLineaventas() {
		return lineaventas;
	}


	public void setLineaventas(List<Lineaventa> lineaventas) {
		this.lineaventas = lineaventas;
	}

	public Lineaventa addLineaventa(Lineaventa lineaventa) {
		getLineaventas().add(lineaventa);
		lineaventa.setVenta(this);
		return lineaventa;
	}

	public Lineaventa removeLineaventa(Lineaventa lineaventa) {
		getLineaventas().remove(lineaventa);
		lineaventa.setVenta(null);
		return lineaventa;
	}

	public final SimpleStringProperty fechaProperty() {
		return this.fecha;
	}

	public final java.lang.String getFecha() {
		return this.fechaProperty().get();
	}

	public final void setFecha(final java.lang.String fecha) {
		this.fechaProperty().set(fecha);
	}



	



	
	/*
	 * public int getIdPedido() { return this.idPedido; }
	 * 
	 * public void setIdPedido(int idPedido) { this.idPedido = idPedido; }
	 * 
	 * 
	 * @Temporal(TemporalType.DATE) public Date getFecha() { return this.fecha;
	 * }
	 * 
	 * public void setFecha(Date fecha) { this.fecha = fecha; }
	 * 
	 * 
	 * public BigDecimal getTotal() { return this.total; }
	 * 
	 * public void setTotal(BigDecimal total) { this.total = total; }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * //bi-directional many-to-one association to Cliente
	 * 
	 * @ManyToOne public Cliente getCliente() { return this.cliente; }
	 * 
	 * public void setCliente(Cliente cliente) { this.cliente = cliente; }
	 * 
	 * 
	 * 
	 * public Empleado getEmpleado() { return this.empleado; }
	 * 
	 * public void setEmpleado(Empleado empleado) { this.empleado = empleado; }
	 */

}