package application.controlador;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.PostConstruct;

import application.modelo.Empleado;
import application.persistencia.EmpleadoDAOJPAImpl;
import application.util.Seguridad;
import io.datafx.controller.FXMLController;
import io.datafx.controller.flow.FlowException;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.action.ActionMethod;
import io.datafx.controller.flow.action.ActionTrigger;
import io.datafx.controller.util.VetoException;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.control.PasswordField;
import javafx.scene.layout.Pane;

@FXMLController(value = "../vista/VistaLogin.fxml")
public class VistaLoginController {
	@FXML
	private ToolBar toolBar;
	@FXML
	private Pane panelIzq;
	@FXML
	private Pane panelDer;
	@FXML
	private PasswordField textPassword;
	@FXML
	private TextField textLegajo;
	@FXML
	@ActionTrigger("test")
	private Button botonAceptar;

	private FlowHandler mainFlowHandler;
	private EmpleadoDAOJPAImpl dao;

	@PostConstruct
	public void init() {
		dao = new EmpleadoDAOJPAImpl();
		
	}

	public void setFlow(FlowHandler flow) {
		this.mainFlowHandler = flow;
	}

	
	public void toRoot() {
		try {
			mainFlowHandler.navigateTo(VistaRootController.class);
		} catch (VetoException | FlowException e) {
			e.printStackTrace();
		}
	}
	
	@ActionMethod("test")
	public void comprobarPass() {
		try {
			Empleado empleado = dao.buscarPorClave(Integer.parseInt(textLegajo.getText()));
			if(empleado.getPassword().equals(Seguridad.SHA1(textPassword.getText()))) {
				System.out.println("entro");
				VistaRootController.setEmpleadoActual(empleado);
				toRoot();
			} else {
				System.out.println(Seguridad.SHA1(textPassword.getText()));
			}
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch(NumberFormatException e) {
			
		} catch(NullPointerException e) {
			
		}
	}
}
